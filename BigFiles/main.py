# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
from pathlib import Path
def main():
    f = open("5Mb.txt", "w+")
    string = 'Utilitatis causa amicitia est quaesita.Lorem ipsum dolor sit amet,\n consectetur adipiscing elit. Collatio igitur ista te nihil iuvat. Honesta oratio, Socratica,\n Platonis etiam. Primum in nostrane potestate est, quid meminerimus? Duo Reges:\n constructio interrete. Quid, si etiam iucunda memoria est praeteritorum malorum? Si quidem, inquit, tollerem, sed relinquo. An nisi populari fama?Quamquam id q\nuidem licebit iis existimare, qui legerint. Summum a vobis bonum voluptas dicitur. At hoc in eo M. Refert tamen, quo modo. Quid sequatur, quid repugnet, vident. Iam id ipsum absurdum\n, maximum malum neglegi.'
    while Path('5Mb.txt').stat().st_size < 500000:
        f.write(string)
        print(Path('5Mb.txt').stat().st_size)
    f.close

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
